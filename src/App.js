// Ant Design
import "antd/dist/antd.css";

// Componentes Propios
import ErrorHandler from "./hoc/errorHandler/ErrorHandler";
import LoadingHandler from "./hoc/loadingHandler/LoadingHandler";
import MainContainer from "./layout/mainContainer/MainContainer";
import Header from "./layout/header/Header";

// DevExtreme
import "devextreme/dist/css/dx.common.css";
import "devextreme/dist/css/dx.light.css";

// Estilos
import "./App.scss";

// Local Storage
import { getLocalStorage } from "./shared/localStorage/LocalStorage";

// React
import React, { useEffect } from "react";

// Redux
import { connect } from "react-redux";
import { setUser } from "./store/actions/UserActions";

const App = ({ setUser }) => {
  useEffect(() => {
    const currentSession = getLocalStorage();
    if (!currentSession || !currentSession.userSession) return;
    setUser(currentSession.userSession);
  }, [setUser]);

  return (
    <LoadingHandler>
      <ErrorHandler>
        <div className="App__container">
          <Header />
          <MainContainer />
        </div>
      </ErrorHandler>
     </LoadingHandler> 
  );
};

export default connect(
  null,
  { setUser }
)(App);
