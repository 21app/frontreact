// Components
import MainContainerCompWrapper from "../../../../layout/components/mainContainerCompWrapper/MainContainerCompWrapper";
import SearchBar from "../../../../shared/components/ui/searchBar/SearchBar";
import NoResultsFound from "../../../../shared/components/ui/noResultsFound/NoResultsFound";
import CardLayout from "../../../../shared/components/cardLayout/CardLayout";
import Card from "../../../../shared/components/ui/card/Card";

// Helpers
import { filterObjectsMethod } from "../../../../shared/helpers/filterMethods";

// React
import React from "react";

// Redux
import { connect } from "react-redux";
import { Searchbar } from "../../../../store/types/UITypes";

const Players = ({ allPlayers, searchedPlayer }) => {
  // Retorna desafíos filtrados
  const getPlayersToShow = () => {
    return filterObjectsMethod(
      allPlayers,
      ["name", "description"],
      searchedPlayer
    );
  };

  return (
    <MainContainerCompWrapper title="Jugadores">
      <SearchBar
        searchBarType={Searchbar.SET_USER_PLAYERS_SEARCHBAR}
        placeHolder="Buscar jugadores"
        value={searchedPlayer}
      />
      {getPlayersToShow().length === 0 ? (
        <NoResultsFound items="jugadores" />
      ) : (
        <CardLayout>
          {getPlayersToShow().map(player => (
            <Card
              key={player.id}
              name={player.name}
              description={player.description}
              image={player.image}
            />
          ))}
        </CardLayout>
      )}
    </MainContainerCompWrapper>
  );
};

const mapStateToProps = state => {
  return {
    allPlayers: state.playersReducer.allPlayers,
    searchedPlayer: state.uiReducer.searchBar.userPlayers
  };
};

export default connect(mapStateToProps)(Players);
