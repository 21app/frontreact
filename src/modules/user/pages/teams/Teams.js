// Components
import MainContainerCompWrapper from "../../../../layout/components/mainContainerCompWrapper/MainContainerCompWrapper";
import SearchBar from "../../../../shared/components/ui/searchBar/SearchBar";
import NoResultsFound from "../../../../shared/components/ui/noResultsFound/NoResultsFound";
import CardLayout from "../../../../shared/components/cardLayout/CardLayout";
import Card from "../../../../shared/components/ui/card/Card";

// Helpers
import { filterObjectsMethod } from "../../../../shared/helpers/filterMethods";

// React
import React from "react";

// Redux
import { connect } from "react-redux";
import { Searchbar } from "../../../../store/types/UITypes";

const Teams = ({ allTeams, searchedTeam }) => {
  // Retorna equipos filtrados
  const getTeamsToShow = () => {
    return filterObjectsMethod(allTeams, ["name", "description"], searchedTeam);
  };

  return (
    <MainContainerCompWrapper title="Equipos">
      <SearchBar
        searchBarType={Searchbar.SET_USER_TEAMS_SEARCHBAR}
        placeHolder="Buscar equipos"
        value={searchedTeam}
      />
      {getTeamsToShow().length === 0 ? (
        <NoResultsFound items="equipos" />
      ) : (
        <CardLayout>
          {getTeamsToShow().map(team => (
            <Card
              key={team.id}
              name={team.name}
              description={team.description}
              image={team.image}
            />
          ))}
        </CardLayout>
      )}
    </MainContainerCompWrapper>
  );
};

const mapStateToProps = state => {
  return {
    allTeams: state.teamsReducer.allTeams,
    searchedTeam: state.uiReducer.searchBar.userTeams
  };
};

export default connect(mapStateToProps)(Teams);
