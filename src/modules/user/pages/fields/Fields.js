// Components
import MainContainerCompWrapper from "../../../../layout/components/mainContainerCompWrapper/MainContainerCompWrapper";
import SearchBar from "../../../../shared/components/ui/searchBar/SearchBar";
import NoResultsFound from "../../../../shared/components/ui/noResultsFound/NoResultsFound";
import CardLayout from "../../../../shared/components/cardLayout/CardLayout";
import Card from "../../../../shared/components/ui/card/Card";

// Helpers
import { filterObjectsMethod } from "../../../../shared/helpers/filterMethods";

// React
import React from "react";

// Redux
import { connect } from "react-redux";
import { Searchbar } from "../../../../store/types/UITypes";

const Fields = ({ allFields, searchedField }) => {
  // Retorna canchas filtradas
  const getFieldsToShow = () => {
    return filterObjectsMethod(
      allFields,
      ["name", "description"],
      searchedField
    );
  };

  return (
    <MainContainerCompWrapper title="Canchas">
      <SearchBar
        searchBarType={Searchbar.SET_USER_FIELDS_SEARCHBAR}
        placeHolder="Buscar canchas"
        value={searchedField}
      />
      {getFieldsToShow().length === 0 ? (
        <NoResultsFound items="canchas" />
      ) : (
        <CardLayout>
          {getFieldsToShow().map(field => (
            <Card
              key={field.id}
              name={field.name}
              description={field.description}
              image={field.image}
            />
          ))}
        </CardLayout>
      )}
    </MainContainerCompWrapper>
  );
};

const mapStateToProps = state => {
  return {
    allFields: state.fieldsReducer.allFields,
    searchedField: state.uiReducer.searchBar.userFields
  };
};

export default connect(mapStateToProps)(Fields);
