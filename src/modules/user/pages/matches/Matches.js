// Components
import MainContainerCompWrapper from "../../../../layout/components/mainContainerCompWrapper/MainContainerCompWrapper";
import SearchBar from "../../../../shared/components/ui/searchBar/SearchBar";
import NoResultsFound from "../../../../shared/components/ui/noResultsFound/NoResultsFound";
import CardLayout from "../../../../shared/components/cardLayout/CardLayout";
import Card from "../../../../shared/components/ui/card/Card";

// Helpers
import { filterObjectsMethod } from "../../../../shared/helpers/filterMethods";

// React
import React from "react";

// Redux
import { connect } from "react-redux";
import { Searchbar } from "../../../../store/types/UITypes";

const Matches = ({ allMatches, searchedMatch }) => {
  // Retorna partidos filtrados
  const getMatchesToShow = () => {
    return filterObjectsMethod(
      allMatches,
      ["name", "description"],
      searchedMatch
    );
  };

  return (
    <MainContainerCompWrapper title="Partidos">
      <SearchBar
        searchBarType={Searchbar.SET_USER_MATCHES_SEARCHBAR}
        placeHolder="Buscar partidos"
        value={searchedMatch}
      />
      {getMatchesToShow().length === 0 ? (
        <NoResultsFound items="partidos" />
      ) : (
        <CardLayout>
          {getMatchesToShow().map(match => (
            <Card
              key={match.id}
              name={match.name}
              description={match.description}
              image={match.image}
            />
          ))}
        </CardLayout>
      )}
    </MainContainerCompWrapper>
  );
};

const mapStateToProps = state => {
  return {
    allMatches: state.matchesReducer.allMatches,
    searchedMatch: state.uiReducer.searchBar.userMatches
  };
};

export default connect(mapStateToProps)(Matches);
