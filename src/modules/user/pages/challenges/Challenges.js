// Components
import MainContainerCompWrapper from "../../../../layout/components/mainContainerCompWrapper/MainContainerCompWrapper";
import SearchBar from "../../../../shared/components/ui/searchBar/SearchBar";
import NoResultsFound from "../../../../shared/components/ui/noResultsFound/NoResultsFound";
import CardLayout from "../../../../shared/components/cardLayout/CardLayout";
import Card from "../../../../shared/components/ui/card/Card";

// Helpers
import { filterObjectsMethod } from "../../../../shared/helpers/filterMethods";

// React
import React from "react";

// Redux
import { connect } from "react-redux";
import { Searchbar } from "../../../../store/types/UITypes";

const Challenges = ({ allChallenges, searchedChallenge }) => {
  // Retorna desafíos filtrados
  const getChallengesToShow = () => {
    return filterObjectsMethod(
      allChallenges,
      ["name", "description"],
      searchedChallenge
    );
  };

  return (
    <MainContainerCompWrapper title="Desafíos">
      <SearchBar
        searchBarType={Searchbar.SET_USER_CHALLENGES_SEARCHBAR}
        placeHolder="Buscar desafíos"
        value={searchedChallenge}
      />
      {getChallengesToShow().length === 0 ? (
        <NoResultsFound items="desafíos" />
      ) : (
        <CardLayout>
          {getChallengesToShow().map(challenge => (
            <Card
              key={challenge.id}
              name={challenge.name}
              description={challenge.description}
              image={challenge.image}
            />
          ))}
        </CardLayout>
      )}
    </MainContainerCompWrapper>
  );
};

const mapStateToProps = state => {
  return {
    allChallenges: state.challengesReducer.allChallenges,
    searchedChallenge: state.uiReducer.searchBar.userChallenges
  };
};

export default connect(mapStateToProps)(Challenges);
