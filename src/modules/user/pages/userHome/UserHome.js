// Components
import MainContainerCompWrapper from "../../../../layout/components/mainContainerCompWrapper/MainContainerCompWrapper";
import UserMap from "../../components/userMap/UserMap";

// Estilos
import "./UserHome.scss";

// React
import React from "react";

// Redux
import { connect } from "react-redux";

const UserHome = ({ user }) => {
  return (
    <MainContainerCompWrapper>
        <UserMap/>
    </MainContainerCompWrapper>
  );
};

const mapStateToProps = state => {
  return {
    user: state.userReducer.user
  };
};

export default connect(mapStateToProps)(UserHome);
