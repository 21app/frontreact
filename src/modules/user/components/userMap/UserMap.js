// Estilos
import "./UserMap.scss";

// Google Maps
import { GoogleMap, withScriptjs, withGoogleMap } from "react-google-maps";

// React
import React from "react";

// Documentation:
// https://www.youtube.com/watch?v=Pf7g32CwX_s
// https://dev.to/jessicabetts/how-to-use-google-maps-api-and-react-js-26c2
// https://console.cloud.google.com/apis/credentials?angularJsUrl=%2Fapis%2Fcredentials%3FcreatingProject%3Dtrue%26angularJsUrl%3D%252Fapis%252Fcredentials%253Fsupportedpurview%253Dproject%2526creatingProject%253Dtrue%26project%3Dapp-258718%26folder%3D%26organizationId%3D%26supportedpurview%3Dproject&project=app-258718&folder=&organizationId=&supportedpurview=project

function Map() {
  return (
    <GoogleMap defaultZoom={8} defaultCenter={{ lat: -34.397, lng: 150.644 }} />
  );
}

const WrappedMap = withScriptjs(withGoogleMap(Map));

export default function UserMap() {

  return (
    <div className="UserMap__container">
      <WrappedMap
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${process.env.REACT_APP_GOOGLE_KEY}`}
        loadingElement={<div style={{ height: "100%" }}></div>}
        containerElement={<div style={{ height: "100%" }}></div>}
        mapElement={<div style={{ height: "100%" }}></div>}
      />
    </div>
  );
}
