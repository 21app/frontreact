// AntDesign
import { Calendar } from "antd";

// Estilos
import "./SideBarCalendar.scss";

// React
import React from "react";

const SideBarCalendar = () => {
  return (
    <div className="SideBarCalendar__container">
      <div className="SideBarCalendar__title">
        <h3>Actividades este mes</h3>
      </div>
      <Calendar fullscreen={false} />
    </div>
  );
};

export default SideBarCalendar;
