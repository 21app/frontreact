// Enums
import { PlayerPositionObjectEnum } from "../../../../shared/enums/PlayerPositionEnum";

// Estilos
import "./SideBarAvatar.scss";

// React
import React from "react";

// React icons
import { TiEdit } from "react-icons/ti";

// Redux
import { connect } from "react-redux";
import { showUserSideDrawer } from "../../../../store/actions/UIActions";

const SideBarAvatar = ({ user, showUserSideDrawer }) => {
  const getPositionName = positionId => {
    return PlayerPositionObjectEnum.find(
      playerPosition => playerPosition.id === positionId
    ).name;
  };

  return (
    <div className="SideBarAvatar__container">
      <div className="SideBarAvatar__button">
        <TiEdit onClick={() => showUserSideDrawer()} />
      </div>
      <div className="SideBarAvatar__avatar-data-container">
        <div className="SideBarAvatar__avatar">
          <img src={user.avatar} alt="avatar" />
        </div>
        <div className="SideBarAvatar__data">
          <p>
            {user.name} {user.age}
          </p>
          <p>{`${getPositionName(user.position)}`}</p>
          <p>Nivel: {user.level}</p>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    user: state.userReducer.user
  };
};

export default connect(
  mapStateToProps,
  { showUserSideDrawer }
)(SideBarAvatar);
