// Enums
import { ActivityTypeObjectEnum } from "../../../../../shared/enums/ActivityTypeEnum";
import { GameTypeObjectEnum } from "../../../../../shared/enums/GameTypeEnum";

// Estilos
import "./SideBarActivityItem.scss";

// React
import React from "react";

// React-icons
import { TiLocationOutline } from "react-icons/ti";

const SideBarActivityItem = ({
  activity: { activityType, gameType, location, date, time }
}) => {
  const getActivityName = activityType => {
    if (!activityType && activityType !== 0) return;
    return ActivityTypeObjectEnum.find(activity => activity.id === activityType)
      .name;
  };

  const getGameTypeName = gameType => {
    if (!gameType && gameType !== 0) return;
    return GameTypeObjectEnum.find(game => game.id === gameType).name;
  };

  return (
    <div className="SideBarActivityItem__container">
      <div className="SideBarActivityItem__top">
        <p>
          {getActivityName(activityType)} de {getGameTypeName(gameType)}
        </p>
        
        <TiLocationOutline/>
      </div>
      <div className="SideBarActivityItem__bottom">
        <small>
          {date} {time}
        </small>
        <small>{location}</small>
      </div>
    </div>
  );
};

export default SideBarActivityItem;
