// Components
import SideBarActivityItem from "./sideBarActivityItem/SideBarActivityItem";

// Enums
import { ActivityTypeEnum } from "../../../../shared/enums/ActivityTypeEnum";
import { GameTypeEnum } from "../../../../shared/enums/GameTypeEnum";

// Estilos
import "./SideBarActivity.scss";

// React
import React from "react";

const activityMockups = [
  {
    id: 1,
    activityType: ActivityTypeEnum.Match,
    gameType: GameTypeEnum.Football,
    location: "Tunal",
    date: "20/12/2019",
    time: "4:00 pm"
  },
  {
    id: 2,
    activityType: ActivityTypeEnum.Match,
    gameType: GameTypeEnum.Microfootball,
    location: "Venecia",
    date: "21/12/2019",
    time: "4:00 pm"
  },
  {
    id: 3,
    activityType: ActivityTypeEnum.Match,
    gameType: GameTypeEnum.Microfootball,
    location: "Venecia",
    date: "21/12/2019",
    time: "6:00 pm"
  },
  {
    id: 4,
    activityType: ActivityTypeEnum.Challenge,
    gameType: GameTypeEnum.Microfootball,
    location: "Villamayor",
    date: "22/12/2019",
    time: "4:00 pm"
  },
  {
    id: 5,
    activityType: ActivityTypeEnum.Match,
    gameType: GameTypeEnum.Football,
    location: "Simón Bolivar",
    date: "26/12/2019",
    time: "4:00 pm"
  },
  {
    id: 6,
    activityType: ActivityTypeEnum.Challenge,
    gameType: GameTypeEnum.Futsal,
    location: "Venecia",
    date: "27/12/2019",
    time: "4:00 pm"
  }
];

const SideBarActivity = () => {
  return (
    <div className="SideBarActivity__container">
      <div className="SideBarActivity__title">
        <h3>Ultimas Actividades</h3>
      </div>
      <div className="SideBarActivity__activity-list">
        {activityMockups.map(activity => (
          <SideBarActivityItem key={activity.id} activity={activity} />
        ))}
      </div>
    </div>
  );
};

export default SideBarActivity;
