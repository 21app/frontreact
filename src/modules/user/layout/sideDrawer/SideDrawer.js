// Ant Design
import { Drawer } from "antd";

// Estilos
import "./SideDrawer.scss";

// React
import React from "react";
import { withRouter } from "react-router-dom";

// Redux
import { connect } from "react-redux";
import { hideUserSideDrawer } from "../../../../store/actions/UIActions";
import { setUserLoggedOut } from "../../../../store/actions/UserActions";

const UserSideDrawer = ({
  showUserSideDrawer,
  hideUserSideDrawer,
  setUserLoggedOut,
  history
}) => {
  const handleLogOut = () => {
    setUserLoggedOut()
    hideUserSideDrawer()
    history.push('/')
  };

  return (
    <Drawer
      title="Basic Drawer"
      placement="right"
      closable={false}
      onClose={hideUserSideDrawer}
      visible={showUserSideDrawer}
    >
      <div className="UserSideDrawer__container__container">
        <button onClick={() => handleLogOut()}>Desloguearse</button>
      </div>
    </Drawer>
  );
};

const mapStateToProps = state => {
  return {
    showUserSideDrawer: state.uiReducer.showUserSideDrawer
  };
};

export default connect(
  mapStateToProps,
  { hideUserSideDrawer, setUserLoggedOut }
)(withRouter(UserSideDrawer));
