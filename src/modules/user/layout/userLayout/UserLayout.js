// Componentes propios
import SideBar from "../sideBar/SideBar";
import SideDrawer from "../sideDrawer/SideDrawer";
import Authentification from "../../../../shared/components/authentification/Authentification";
import UserHome from "../../pages/userHome/UserHome";
import Matches from "../../pages/matches/Matches";
import Challenges from "../../pages/challenges/Challenges";
import Fields from "../../pages/fields/Fields";
import Teams from "../../pages/teams/Teams";
import Players from "../../pages/players/Players";

// Estilos
import "./UserLayout.scss";

// React
import React, { useEffect } from "react";
import { Route, withRouter } from "react-router-dom";

// Redux
import { connect } from "react-redux";
import { setAllChallenges } from "../../../../store/actions/ChallengesActions";
import { setAllFields } from "../../../../store/actions/FieldsActions";
import { setAllMatches } from "../../../../store/actions/MatchesActions";
import { setAllPlayers } from "../../../../store/actions/PlayersActions";
import { setAllTeams } from "../../../../store/actions/TeamsActions";

const UserLayout = ({
  setAllChallenges,
  setAllFields,
  setAllMatches,
  setAllPlayers,
  setAllTeams,
  match
}) => {
  useEffect(() => {
    setAllChallenges();
    setAllFields();
    setAllMatches();
    setAllPlayers();
    setAllTeams();
  }, [
    setAllChallenges,
    setAllFields,
    setAllMatches,
    setAllPlayers,
    setAllTeams
  ]);

  return (
    <Authentification>
      <div className="UserHome__container">
        <SideDrawer/>
        <SideBar />
        <Route
          path={match.path}
          exact
          render={() => (
            <Authentification>
              <UserHome />
            </Authentification>
          )}
        />
        <Route
          path={match.path + "/matches"}
          render={() => (
            <Authentification>
              <Matches />
            </Authentification>
          )}
        />
        <Route
          path={match.path + "/challenges"}
          render={() => (
            <Authentification>
              <Challenges />
            </Authentification>
          )}
        />
        <Route
          path={match.path + "/fields"}
          render={() => (
            <Authentification>
              <Fields />
            </Authentification>
          )}
        />
        <Route
          path={match.path + "/teams"}
          render={() => (
            <Authentification>
              <Teams />
            </Authentification>
          )}
        />
        <Route
          path={match.path + "/players"}
          render={() => (
            <Authentification>
              <Players />
            </Authentification>
          )}
        />
      </div>
    </Authentification>
  );
};

export default connect(
  null,
  {
    setAllChallenges,
    setAllFields,
    setAllMatches,
    setAllPlayers,
    setAllTeams
  }
)(withRouter(UserLayout));
