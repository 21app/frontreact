// Componentes Propios
import SideBarAvatar from "../../components/sideBarAvatar/SideBarAvatar";
import SideBarActivity from "../../components/sideBarActivity/SideBarActivity";
import SideBarCalendar from "../../components/sideBarCalendar/SideBarCalendar";

// Estilos
import "./SideBar.scss";

// React
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";

// Redux
import { connect } from 'react-redux'

const SideBar = ({ history, isUserLoggedIn }) => {
  const [showSideBarState, setShowSideBarState] = useState(false);

  useEffect(() => {
    if (history.location.pathname === "/") {
      if(isUserLoggedIn){
        return setShowSideBarState(true);  
      }
      return setShowSideBarState(false);
    }    
    setShowSideBarState(true);
  }, [history.location.pathname, isUserLoggedIn]);

  const getSideDrawer = () => {
    if (!showSideBarState) {
      return null;
    }
    return (
      <div className="SideBar__container">
        <div className="SideBar__avatar-component">
          <SideBarAvatar />
        </div>
        <div className="SideBar__activity-component">
          <SideBarActivity />
        </div>
        <div className="SideBar__calendar-component">
          <SideBarCalendar />
        </div>
      </div>
    );
  };

  return getSideDrawer();
};

const mapStateToProps = state => {
  return ({
    isUserLoggedIn: state.userReducer.isUserLoggedIn
  })
}

export default connect(mapStateToProps)(withRouter(SideBar));
