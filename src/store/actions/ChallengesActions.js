// Enums
import { ResourceEnum } from "../../shared/enums/ResourceEnum";

// Mockups
import { ChallengesMockup } from '../../mockups/ChallengesMockup'

// Types
import * as actionTypes from "../types/ChallengesTypes";

// UiActions
import * as UiActions from "./UIActions";

export const setAllChallenges = () => dispatch => {
  dispatch({
    type: actionTypes.GET_ALL_CHALLENGES,
    payload: ChallengesMockup
  });
  setTimeout(() => {
    dispatch(UiActions.disablePageLoading(ResourceEnum.challenges));
  }, [1000]);
};
