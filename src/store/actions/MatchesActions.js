// Enums
import { ResourceEnum } from "../../shared/enums/ResourceEnum";

// Mockups
import { MatchesMockup } from '../../mockups/MatchesMockup'

// Types
import * as actionTypes from "../types/MatchesTypes";

// UiActions
import * as UiActions from "./UIActions";

export const setAllMatches = () => dispatch => {
  dispatch({
    type: actionTypes.GET_ALL_MATCHES,
    payload: MatchesMockup
  });
  setTimeout(() => {
    dispatch(UiActions.disablePageLoading(ResourceEnum.matches));
  }, [3000]);
};
