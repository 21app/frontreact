// Enums
import { PlayerPositionEnum } from "../../shared/enums/PlayerPositionEnum";

// LocalStorage
import { setLocalStorage } from "../../shared/localStorage/LocalStorage";

// Password encryption
import { SHA256 } from "crypto-js";

// Types
import * as actionTypes from "../types/UserTypes";

const dummyAvatar = "https://picsum.photos/seed/picsum/200/300";

export const setUser = user => dispatch => {
  const encriptedUser = {
    ...user,
    password: SHA256(user.password),
    name: "Cristian Mateus",
    email: "mateus_09@live.com",
    avatar: dummyAvatar,
    age: 23,
    position: PlayerPositionEnum.Midfielder,
    level: 3
  };
  setLocalStorage({ userSession: encriptedUser });
  dispatch(setUserLoggedIn());
  dispatch({
    type: actionTypes.SET_USER,
    payload: encriptedUser
  });
};

export const setUserLoggedIn = () => dispatch => {
  dispatch({
    type: actionTypes.SET_LOGGED_IN
  });
};

export const setUserLoggedOut = () => dispatch => {
  setLocalStorage(null);
  dispatch({
    type: actionTypes.SET_LOGGED_OUT
  });
};
