// Enum
import { ResourceEnum } from "../../shared/enums/ResourceEnum";

// Actions
import * as actionTypes from "../types/UITypes";

export const setToast = (toastType, toastMessage) => dispatch => {
  dispatch({
    type: actionTypes.SET_TOAST,
    payload: {
      type: toastType,
      message: toastMessage
    }
  });
};

export const disactivateWelcomeMessage = () => dispatch => {
  dispatch({
    type: actionTypes.DISACTIVATE_WELCOME_MESSAGE
  });
};

export const disablePageLoading = pageResource => dispatch => {
  let pageLoadingObject = {};
  switch (pageResource) {
    case ResourceEnum.challenges:
      pageLoadingObject = { challengesPageLoading: false };
      break;
    case ResourceEnum.fields:
      pageLoadingObject = { fieldsPageLoading: false };
      break;
    case ResourceEnum.matches:
      pageLoadingObject = { matchesPageLoading: false };
      break;
    case ResourceEnum.players:
      pageLoadingObject = { playersPageLoading: false };
      break;
    case ResourceEnum.teams:
      pageLoadingObject = { teamsPageLoading: false };
      break;
    default:
      pageLoadingObject = { ...pageLoadingObject };
  }
  dispatch({
    type: actionTypes.DISACTIVATE_PAGE_LOADING,
    payload: pageLoadingObject
  });
};

export const showUserSideDrawer = () => dispatch => {
  dispatch({
    type: actionTypes.SHOW_USER_SIDEDRAWER
  });
};

export const hideUserSideDrawer = () => dispatch => {
  dispatch({
    type: actionTypes.HIDE_USER_SIDEDRAWER
  });
};

export const setSearchBar = (page, searchValue) => dispatch => {
  let searchBarObject = {};
  switch (page) {
    case actionTypes.Searchbar.SET_USER_MATCHES_SEARCHBAR:
      searchBarObject = { userMatches: searchValue };
      break;
    case actionTypes.Searchbar.SET_USER_CHALLENGES_SEARCHBAR:
      searchBarObject = { userChallenges: searchValue };
      break;
    case actionTypes.Searchbar.SET_USER_FIELDS_SEARCHBAR:
      searchBarObject = { userFields: searchValue };
      break;
    case actionTypes.Searchbar.SET_USER_TEAMS_SEARCHBAR:
      searchBarObject = { userTeams: searchValue };
      break;
    case actionTypes.Searchbar.SET_USER_PLAYERS_SEARCHBAR:
      searchBarObject = { userPlayers: searchValue };
      break;
    default:
      return;
  }
  dispatch({
    type: actionTypes.SET_SEARCHBAR,
    payload: searchBarObject
  });
};
