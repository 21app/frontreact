// Enums
import { ResourceEnum } from "../../shared/enums/ResourceEnum";

// Mockups
import { PlayersMockup } from '../../mockups/PlayersMockup'

// Types
import * as actionTypes from "../types/PlayersTypes";

// UiActions
import * as UiActions from "./UIActions";

export const setAllPlayers = () => dispatch => {
  dispatch({
    type: actionTypes.GET_ALL_PLAYERS,
    payload: PlayersMockup
  });
  setTimeout(() => {
    dispatch(UiActions.disablePageLoading(ResourceEnum.players));
  }, [4000]);
};
