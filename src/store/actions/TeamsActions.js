// Enums
import { ResourceEnum } from "../../shared/enums/ResourceEnum";

// Mockups
import { TeamsMockup } from '../../mockups/TeamsMockup'

// Types
import * as actionTypes from "../types/TeamsTypes";

// UiActions
import * as UiActions from "./UIActions";

export const setAllTeams = () => dispatch => {
  dispatch({
    type: actionTypes.GET_ALL_TEAMS,
    payload: TeamsMockup
  });
  setTimeout(() => {
    dispatch(UiActions.disablePageLoading(ResourceEnum.teams));
  }, [5000]);
};
