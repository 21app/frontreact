// Enums
import { ResourceEnum } from "../../shared/enums/ResourceEnum";

// Mockups
import { FieldsMockup } from '../../mockups/FieldsMockup'

// Types
import * as actionTypes from "../types/FieldsTypes";

// UiActions
import * as UiActions from "./UIActions";

export const setAllFields = () => dispatch => {
  dispatch({
    type: actionTypes.GET_ALL_FIELDS,
    payload: FieldsMockup
  });
  setTimeout(() => {
    dispatch(UiActions.disablePageLoading(ResourceEnum.fields));
  }, [2000]);
};
