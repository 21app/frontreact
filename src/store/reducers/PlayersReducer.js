import * as actionTypes from "../types/PlayersTypes";

const initialState = {
  allPlayers: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_PLAYERS:
      return { ...initialState, allPlayers: action.payload };
    default:
      return state;
  }
};

export default reducer;
