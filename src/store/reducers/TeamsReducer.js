import * as actionTypes from "../types/TeamsTypes";

const initialState = {
  allTeams: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_TEAMS:
      return { ...initialState, allTeams: action.payload };
    default:
      return state;
  }
};

export default reducer;
