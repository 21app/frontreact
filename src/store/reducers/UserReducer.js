import * as actionTypes from "../types/UserTypes";

const initialState = {
  user: null,
  isUserLoggedIn: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_USER:
      return { ...state, user: action.payload };
    case actionTypes.SET_LOGGED_IN:
      return { ...state, isUserLoggedIn: true };
    case actionTypes.SET_LOGGED_OUT:
      return { ...state, isUserLoggedIn: false };
    default:
      return state;
  }
};

export default reducer;
