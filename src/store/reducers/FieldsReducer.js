import * as actionTypes from "../types/FieldsTypes";

const initialState = {
  allFields: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_FIELDS:
      return { ...initialState, allFields: action.payload };
    default:
      return state;
  }
};

export default reducer;
