import * as actionTypes from "../types/ChallengesTypes";

const initialState = {
  allChallenges: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_CHALLENGES:
      return { ...initialState, allChallenges: action.payload };
    default:
      return state;
  }
};

export default reducer;
