import * as actionTypes from "../types/MatchesTypes";

const initialState = {
  allMatches: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_MATCHES:
      return { ...initialState, allMatches: action.payload };
    default:
      return state;
  }
};

export default reducer;
