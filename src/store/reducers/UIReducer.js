import * as actionTypes from "../types/UITypes";

const initialState = {
  toast: {
    type: null,
    message: null
  },
  pagesLoading: {
    challengesPageLoading: true,
    fieldsPageLoading: true,
    matchesPageLoading: true,
    playersPageLoading: true,
    teamsPageLoading: true
  },
  searchBar: {
    userMatches: null,
    userChallenges: null,
    userFields: null,
    userTeams: null,
    userPlayers: null
  },
  error: null,
  showWelcomeMessage: true,
  showUserSideDrawer: false
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_TOAST:
      return { ...state, toast: action.payload };
    case actionTypes.DISACTIVATE_WELCOME_MESSAGE:
      return { ...state, showWelcomeMessage: false };
    case actionTypes.DISACTIVATE_PAGE_LOADING:
      return {
        ...state,
        pagesLoading: { ...state.pagesLoading, ...action.payload }
      };
    case actionTypes.SHOW_USER_SIDEDRAWER:
      return { ...state, showUserSideDrawer: true };
    case actionTypes.HIDE_USER_SIDEDRAWER:
      return { ...state, showUserSideDrawer: false };
    case actionTypes.SET_SEARCHBAR:
      return { ...state, searchBar: { ...state.searchBar, ...action.payload } };
    default:
      return state;
  }
};

export default reducer;
