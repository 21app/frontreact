// Estilos
import "./HeaderAvatar.scss";

// React
import React from "react";

// Redux
import { connect } from "react-redux";
import { showUserSideDrawer } from "../../../store/actions/UIActions";

const HeaderAvatar = ({ user, showUserSideDrawer }) => {
  return (
    <div
      className="HeaderAvatar__container"
      onClick={() => showUserSideDrawer()}
    >
      <p>{user.name}</p>
      <div
        className="HeaderAvatar__avatar"
        style={{ backgroundImage: `url('${user.avatar}')` }}
      ></div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    user: state.userReducer.user
  };
};

export default connect(
  mapStateToProps,
  { showUserSideDrawer }
)(HeaderAvatar);
