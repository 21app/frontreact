// Ant Design
import { Modal } from "antd";

// DevExpress
// Documentation:
// https://js.devexpress.com/Demos/WidgetsGallery/Demo/Common/FormsAndMultiPurposeOverview/React/Light/
// https://js.devexpress.com/Documentation/ApiReference/UI_Widgets/dxForm/Item_Types/SimpleItem/
import { Button } from "devextreme-react";
import Form, {
  SimpleItem,
  ButtonItem,
  GroupItem,
  Label,
  RequiredRule,
  EmailRule
} from "devextreme-react/form";

// Estilos
import "./HeaderLogRegister.scss";

// React
import React, { useState } from "react";
import { withRouter } from 'react-router-dom'

// Redux
import { connect } from 'react-redux'
import { setUser } from '../../../store/actions/UserActions'

const HeaderLogRegister = ({setUser, history}) => {
  const [showSignUpModalState, setShowSignUpModalState] = useState(false);
  const [showSignInModalState, setShowSignInModalState] = useState(false);

  const [signUpFormState] = useState({
    email: "",
    password: ""
  });

  const [signInFormState] = useState({
    email: "",
    password: ""
  });

  const handleCloseModals = () => {
    setShowSignUpModalState(false);
    setShowSignInModalState(false);
  };

  const handleSignUp = (e) => {
    e.preventDefault()
    console.log(signUpFormState)
  }

  const handleSignIn = (e) => {
    e.preventDefault()
    setUser(signInFormState)
    history.push('/user')
  }

  const getPageModals = () => {
    return (
      <React.Fragment>
        {/* Registrarse */}
        <Modal
          title="Crear cuenta"
          visible={showSignUpModalState}
          onOk={() => setShowSignUpModalState(false)}
          onCancel={() => setShowSignUpModalState(false)}
          footer={false}
        >
          <form onSubmit={(e) => handleSignUp(e)}>
            <Form
              formData={signUpFormState}
              showColonAfterLabel={false}
              labelLocation={"top"}
            >
              <GroupItem>
                {/* Correo */}
                <SimpleItem
                  dataField={"email"}
                  editorType={"dxTextBox"}
                  editorOptions={{
                    stylingMode: "filled",
                    inputAttr: {
                      autocomplete: "email"
                    }
                  }}
                >
                  <Label text={"Correo electrónico"} />
                  <EmailRule message="Debe ser un correo" />
                  <RequiredRule message="Campo requerido" />
                </SimpleItem>
                {/* Contraseña */}
                <SimpleItem
                  dataField={"password"}
                  editorType={"dxTextBox"}
                  editorOptions={{
                    mode: "password",
                    stylingMode: "filled"
                  }}
                >
                  <Label text={"Contraseña"} />
                  <RequiredRule message="Campo requerido" />
                </SimpleItem>
                {/* Botones */}
                <GroupItem
                  colCount="2"
                  cssClass="HeaderLogRegister__modal-buttons"
                >
                  <ButtonItem
                    horizontalAlignment="center"
                    buttonOptions={{
                      text: "Continuar",
                      width: 120,
                      type: "success",
                      useSubmitBehavior: false,
                      stylingMode: "outlined"
                    }}
                  />
                  <ButtonItem
                    horizontalAlignment="center"
                    buttonOptions={{
                      text: "Cancelar",
                      width: 120,
                      type: "danger",
                      useSubmitBehavior: false,
                      stylingMode: "outlined",
                      onClick: handleCloseModals
                    }}
                  />
                </GroupItem>
              </GroupItem>
            </Form>
          </form>
        </Modal>
        {/* Loguearse */}
        <Modal
          title="Ingresar a cuenta"
          visible={showSignInModalState}
          onOk={() => setShowSignInModalState(false)}
          onCancel={() => setShowSignInModalState(false)}
          footer={false}
        >
          <form onSubmit={(e) => handleSignIn(e)}>
            <Form
              formData={signInFormState}
              showColonAfterLabel={false}
              labelLocation={"top"}
            >
              <GroupItem>
                {/* Correo */}
                <SimpleItem
                  dataField={"email"}
                  editorType={"dxTextBox"}
                  editorOptions={{
                    stylingMode: "filled",
                    inputAttr: {
                      autocomplete: "email"
                    }
                  }}
                >
                  <Label text={"Correo electrónico"} />
                  <EmailRule message="Debe ser un correo" />
                  <RequiredRule message="Campo requerido" />
                </SimpleItem>
                {/* Contraseña */}
                <SimpleItem
                  dataField={"password"}
                  editorType={"dxTextBox"}
                  editorOptions={{
                    mode: "password",
                    stylingMode: "filled"
                  }}
                >
                  <Label text={"Contraseña"} />
                  <RequiredRule message="Campo requerido" />
                </SimpleItem>
                {/* Botones */}
                <GroupItem
                  colCount="2"
                  cssClass="HeaderLogRegister__modal-buttons"
                >
                  <ButtonItem
                    horizontalAlignment="center"
                    buttonOptions={{
                      text: "Continuar",
                      type: "success",
                      useSubmitBehavior: true,
                      stylingMode: "outlined",
                    }}
                  />
                  <ButtonItem
                    horizontalAlignment="center"
                    buttonOptions={{
                      text: "Cancelar",
                      type: "danger",
                      useSubmitBehavior: false,
                      stylingMode: "outlined",
                      onClick: handleCloseModals
                    }}
                  />
                </GroupItem>
              </GroupItem>
            </Form>
          </form>
        </Modal>
      </React.Fragment>
    );
  };

  return (
    <React.Fragment>
      {getPageModals()}
      <div className="HeaderLogRegister__container">
        <Button
          type={"default"}
          text={"Registrar"}
          className="HeaderLogRegister__button"
          onClick={() => setShowSignUpModalState(true)}
        />
        <Button
          type={"success"}
          text={"Ingresar"}
          className="HeaderLogRegister__button"
          onClick={() => setShowSignInModalState(true)}
        />
      </div>
    </React.Fragment>
  );
};

export default connect(null, { setUser })(withRouter(HeaderLogRegister));
