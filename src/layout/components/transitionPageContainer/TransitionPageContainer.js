// Estilos
import "./TransitionPageContainer.scss";

// React
import React from "react";

const TransitionPageContainer = ({ children }) => {
  return <div className="TransitionPageContainer__container">{children}</div>;
};

export default TransitionPageContainer;
