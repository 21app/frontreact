// Estilos
import "./HeaderLogo.scss";

// React
import React from "react";
import { withRouter } from "react-router-dom";

// Redux
import { connect } from "react-redux";

const logoImage = "./assets/images/corporateLogo.png";

const HeaderLogo = ({ isUserLoggedIn, history }) => {
  return (
    <div className="HeaderLogo__container">
      <img
        src={logoImage}
        alt="header-logo"
        onClick={() => {
          isUserLoggedIn ? history.push("/user") : history.push("/");
        }}
      />
    </div>
  );
};

const mapStateToProps = state => {
  return {
    isUserLoggedIn: state.userReducer.isUserLoggedIn
  };
};

export default connect(mapStateToProps)(withRouter(HeaderLogo));
