// Estilos
import "./HeaderNavBar.scss";

// React
import React from "react";
import { withRouter, NavLink } from "react-router-dom";

const HeaderNavBar = () => {
  const navRedirections = [
    {
      name: "Partidos",
      path: "/matches"
    },
    {
      name: "Desafíos",
      path: "/challenges"
    },
    {
      name: "Canchas",
      path: "/fields"
    },
    {
      name: "Equipos",
      path: "/teams"
    },
    {
      name: "Jugadores",
      path: "/players"
    }
  ];
  return (
    <div className="HeaderNavBar__container">
      {navRedirections.map((navRedirection, i) => (
        <NavLink key={i} to={`/user${navRedirection.path}`} activeClassName="chosen">
          <div
            className={
              i > 0 && i < navRedirections.length - 1
                ? "HeaderNavBar__nav"
                : i === 0 ? "HeaderNavBar__nav-left":"HeaderNavBar__nav-right"
            }
          >
            <p>{navRedirection.name}</p>
          </div>
        </NavLink>
      ))}
    </div>
  );
};

export default withRouter(HeaderNavBar);
