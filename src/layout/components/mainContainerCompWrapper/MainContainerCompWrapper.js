// Estilos
import './MainContainerCompWrapper.scss'

// React
import React from "react";

const MainContainerCompWrapper = ({ title, children }) => {
  return (
    <div className="MainContainerCompWrapper__container">
      <div className="MainContainerCompWrapper__title">
        <h2>{title}</h2>
      </div>
      <div className="MainContainerCompWrapper__children">{children}</div>
    </div>
  );
};

export default MainContainerCompWrapper;
