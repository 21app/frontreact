// Componentes
import HeaderLogo from "../components/headerLogo/HeaderLogo";
import HeaderNavBar from "../components/headerNavBar/HeaderNavBar";
import HeaderAvatar from "../components/headerAvatar/HeaderAvatar";
import HeaderLogRegister from "../components/headerLogRegister/HeaderLogRegister";

// Estilos
import "./Header.scss";

// React
import React from "react";

// Redux
import { connect } from "react-redux";

const Header = ({ isUserLoggedIn }) => {
  return (
    <div className="Header__container">
      <HeaderLogo />
      <HeaderNavBar />
      {isUserLoggedIn ? <HeaderAvatar /> : <HeaderLogRegister />}
    </div>
  );
};

const mapStateToProps = state => {
  return {
    isUserLoggedIn: state.userReducer.isUserLoggedIn
  };
};

export default connect(mapStateToProps)(Header);
