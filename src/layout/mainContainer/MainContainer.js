// Componentes Propios
import NoAccess from "../../shared/pages/noAccess/NoAccess";
import NotFound404 from "../../shared/pages/notFound404/NotFound404";
import NotFound404Handler from "../../shared/pages/notFound404Handler/NotFound404Handler";
import Welcome from "../../shared/pages/welcome/Welcome";
import UserLayout from "../../modules/user/layout/userLayout/UserLayout";

// Estilos
import "./MainContainer.scss";

// React
import React from "react";
import { Switch, Route, withRouter } from "react-router-dom";

// React Page Transition
import { CSSTransition, TransitionGroup } from "react-transition-group";
import TransitionPageContainer from "../components/transitionPageContainer/TransitionPageContainer";

const MainContainer = () => {
  return (
    <div className="MainContainer__container">
      <div className="MainContainer__rigth">
        <Route
          render={({ location }) => (
            <TransitionGroup className="MainContainer__transition">
              <CSSTransition key={location.key} timeout={450} classNames="fade">
                <TransitionPageContainer>
                  <Switch location={location}>
                    <Route path="/user" component={UserLayout} />
                    <Route path="/noAccess" component={NoAccess} />
                    <Route path="/404" exact component={NotFound404} />
                    <Route path="/" exact component={Welcome} />
                    <Route component={NotFound404Handler} />
                  </Switch>
                </TransitionPageContainer>
              </CSSTransition>
            </TransitionGroup>
          )}
        />
      </div>
    </div>
  );
};

export default withRouter(MainContainer);
