// React
import React, { Fragment, useEffect } from "react";

// Enum
import { ToastTypeEnum } from "../../shared/enums/ToastTypeEnum";

// Redux
import { connect } from "react-redux";

// Toast
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
toast.configure();

const ErrorHandler = ({ children, toastRedux }) => {
  useEffect(() => {
    if (!toastRedux.type || !toastRedux.message) {
      return;
    }
    switch (toastRedux.type) {
      case ToastTypeEnum.Error:
        toast.error(toastRedux.message, {
          autoClose: 4000,
          position: toast.POSITION.BOTTOM_RIGHT
        });
        break;
      case ToastTypeEnum.Warning:
        toast.warn(toastRedux.message, {
          autoClose: 4000,
          position: toast.POSITION.BOTTOM_RIGHT
        });
        break;
      case ToastTypeEnum.Success:
        toast.success(toastRedux.message, {
          autoClose: 1900,
          position: toast.POSITION.BOTTOM_RIGHT
        });
        break;
      default:
        break;
    }
  }, [toastRedux]);

  return <Fragment>{children}</Fragment>;
};

const mapStateToProps = state => {
  return {
    toastRedux: state.uiReducer.toast
  };
};

export default connect(mapStateToProps)(ErrorHandler);
