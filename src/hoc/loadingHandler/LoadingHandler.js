// Componentes propios
import PageLoader from "../../shared/components/ui/pageLoader/PageLoader";

// React
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";

// Redux
import { connect } from "react-redux";

const LoadingHandler = ({
  children,
  pagesLoading,
  isUserLoggedIn,
  history
}) => {
  const [arePagesLoadingState, setArePagesLoadingState] = useState(true);

  useEffect(() => {
    const arePagesLoading = Object.values(pagesLoading).filter(
      pageLoading => pageLoading === true
    );
    if (arePagesLoading.length > 0) {
      return setArePagesLoadingState(true);
    } else {
      return setArePagesLoadingState(false);
    }
  }, [pagesLoading]);

  // Determina si mostrar o ocultar el loader a partir de la página
  const showPageLoader = () => {
    switch (history.location.pathname) {
      case "/":
        return null;
      case "/noAccess":
        return null;
      case "/404":
        return null;
      default:
        if (arePagesLoadingState) {
          return <PageLoader />;
        }
        return null;
    }
  };

  return (
    <React.Fragment>
      {showPageLoader()}
      {children}
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    pagesLoading: state.uiReducer.pagesLoading,
    isUserLoggedIn: state.userReducer.isUserLoggedIn
  };
};

export default connect(mapStateToProps)(withRouter(LoadingHandler));
