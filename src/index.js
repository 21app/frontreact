import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import * as serviceWorker from "./serviceWorker";

import { ConfigProvider } from "antd";
import esEs from "antd/es/locale/es_ES";

import { BrowserRouter } from "react-router-dom";

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import thunk from "redux-thunk";

import UIReducer from "./store/reducers/UIReducer";
import UserReducer from "./store/reducers/UserReducer";
import ChallengesReducer from "./store/reducers/ChallengesReducer";
import FieldsReducer from "./store/reducers/FieldsReducer";
import MatchesReducer from "./store/reducers/MatchesReducer";
import PlayersReducer from "./store/reducers/PlayersReducer";
import TeamsReducer from "./store/reducers/TeamsReducer";

require('dotenv').config()

const rootReducer = combineReducers({
  uiReducer: UIReducer,
  userReducer: UserReducer,
  challengesReducer: ChallengesReducer,
  fieldsReducer: FieldsReducer,
  matchesReducer: MatchesReducer,
  playersReducer: PlayersReducer,
  teamsReducer: TeamsReducer
});

const composeEnchancers =
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  composeEnchancers(applyMiddleware(thunk))
);

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <ConfigProvider locale={esEs}>
        <App />
      </ConfigProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
