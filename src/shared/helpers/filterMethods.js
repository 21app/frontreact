// Methods that helper uses
const doesObjectHaveString = (object, objectKeys, filterValue) => {
  let validKeys = [];
  objectKeys.forEach(key => {
    // Obtener valor que concuerde usando metodo match de javascript
    // revisar video:
    // https://www.youtube.com/watch?v=W7S_Vmq0GSs
    const matchedValue = object[key]
      .toLowerCase()
      .match(filterValue.toLowerCase());
    // Si no concuerda el valor o concuerda pero no el indice indicado:
    if (!matchedValue || matchedValue.index !== 0) return;
    validKeys.push(object[key]);
  });
  if (validKeys.length === 0) {
    // Si no se encontraron llaves validas
    return false;
  } else {
    // Si se encontraron llaves validas
    return true;
  }
};

// Se necesita enviar:
// 1.objectArray = Lista de objetos para filtrar
// 2.filtersArray = Lista de filtros(tipo string)
export const filterObjectsMethod = (objectArray, filtersArray, filterValue) => {
  if (!objectArray) return;
  if (objectArray.length === 0 || !filterValue) return objectArray;
  // 1. filtrar el arreglo
  const filteredArray = objectArray.filter(object => {
    if(!doesObjectHaveString(object, filtersArray, filterValue))return null
    return object
  }
  );
  // 2. Retornar arreglo filtrado
  return filteredArray;
};
