export const ResourceEnum = {
    challenges: 0,
    fields: 1,
    matches: 2,
    players: 3,
    teams: 4
}