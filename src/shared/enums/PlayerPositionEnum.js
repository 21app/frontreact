export const PlayerPositionEnum = {
  GoalKeeper: 0,
  Defense: 1,
  Wing: 2,
  Midfielder: 3,
  Striker: 4
};

export const PlayerPositionObjectEnum = [
  { id: PlayerPositionEnum.GoalKeeper, name: "Portero" },
  { id: PlayerPositionEnum.Defense, name: "Defensa" },
  { id: PlayerPositionEnum.Wing, name: "Volante" },
  { id: PlayerPositionEnum.Midfielder, name: "Mediocampista" },
  { id: PlayerPositionEnum.Striker, name: "Delantero" }
];
