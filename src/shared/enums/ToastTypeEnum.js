export const ToastTypeEnum = {
    Error: 0,
    Warning: 1,
    Success: 2
}