export const GameTypeEnum = {
    Football: 0,
    Microfootball: 1,
    Futsal: 2
}

export const GameTypeObjectEnum = [
    {
        id: GameTypeEnum.Football,
        name: "Fútbol"
    },
    {
        id: GameTypeEnum.Microfootball,
        name: "Microfútbol"
    },
    {
        id: GameTypeEnum.Futsal,
        name: "Futsal"
    }
]