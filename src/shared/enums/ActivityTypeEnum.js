export const ActivityTypeEnum = {
    Match: 0,
    Challenge: 1
}

export const ActivityTypeObjectEnum = [
    {
        id: ActivityTypeEnum.Match,
        name: "Partido"
    },
    {
        id: ActivityTypeEnum.Challenge,
        name: "Desafío"
    }
]