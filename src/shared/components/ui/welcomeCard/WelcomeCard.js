// AntDesign
import { Tooltip } from "antd";

// Estilos
import "./WelcomeCard.scss";

// React
import React from "react";
import { withRouter } from "react-router-dom";

const WelcomeCard = ({ cardObject, history }) => {
  return (
    <Tooltip placement="bottom" title={<span>{cardObject.description}</span>}>
      <div
        className="WelcomeCard__container"
        onClick={() => history.push(`user${cardObject.path}`)}
      >
        <h1>{cardObject.name}</h1>
        {cardObject.icon}
      </div>
    </Tooltip>
  );
};

export default withRouter(WelcomeCard);
