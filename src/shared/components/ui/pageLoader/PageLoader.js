// Estilos
import "./PageLoader.scss";

// React
import React from "react";

const PageLoader = () => {
  return (
    <div className="PageLoader__container">
      <div className="PageLoader__loader-container">
        <div className="PageLoader__loader"></div>
        <div className="PageLoader__shadow"></div>
      </div>
      <div className="PageLoader__logo-container">
        <h1>21App</h1>
      </div>
    </div>
  );
};

export default PageLoader;
