// Estilos
import "./Card.scss";

// React
import React from "react";

const Card = ({ name, image, description }) => {
  return (
    <div className="Card__container">
      <div className="Card__title">
        <p>{name}</p>
      </div>
      <div className="Card__image" style={{backgroundImage: `url('${image}')`}}>
        {/* <img alt="" src={image} /> */}
      </div>
      <div className="Card__description">
        <p>{description}</p>
      </div>
    </div>
  );
};

export default Card;
