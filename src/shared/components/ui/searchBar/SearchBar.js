// Estilos
import "./SearchBar.scss";

// Redux
import { connect } from "react-redux";
import { setSearchBar } from "../../../../store/actions/UIActions";

// React
import React, { useRef, useEffect } from "react";

const SearchBar = ({ searchBarType, placeHolder, value, setSearchBar }) => {
  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const inputRef = useRef();

  return (
    <div className="SearchBar__container">
      <input
        ref={inputRef}
        type="text"
        placeholder={placeHolder}
        value={value? value: "" }
        onChange={e => setSearchBar(searchBarType, e.target.value)}
      />
    </div>
  );
};

export default connect(null, { setSearchBar })(SearchBar);
