// Estilos
import './NoResultsFound.scss'

// React
import React from "react";

const NoResultsFound = ({ items }) => {
  return (
    <div className="NoResultsFound__container">
      <p>No se encontraron {items? items: 'registros'}</p>
    </div>
  );
};

export default NoResultsFound