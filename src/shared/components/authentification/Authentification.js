// Componentes Propios
import PageLoader from "../../components/ui/pageLoader/PageLoader";

// Local Storage
import { getLocalStorage } from '../../localStorage/LocalStorage'

// React
import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";

// Redux
import { connect } from "react-redux";

const Authentification = ({ children, history }) => {

  useEffect(() => {
    const currentSession = getLocalStorage()
    if(!currentSession){
      return history.push('/noAccess') 
    }
    if(!currentSession.userSession){
      return history.push('/noAccess')      
    }
    setDoesUserHaveAccessState(true)
  }, [history]);

  const [doesUserHaveAccessState, setDoesUserHaveAccessState] = useState(false);

  return (  
    <React.Fragment>
      {doesUserHaveAccessState? children: <PageLoader />}
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    isUserLoggedIn: state.userReducer.isUserLoggedIn
  };
};

export default connect(mapStateToProps)(withRouter(Authentification));
