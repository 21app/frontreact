export const getLocalStorage = () => {
    return JSON.parse(localStorage.getItem('storage'))
}

export const setLocalStorage = (storage) => {
    localStorage.setItem('storage', JSON.stringify(storage))
}