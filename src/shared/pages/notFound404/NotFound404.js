// Estilos
import './NotFound404.scss'

// React
import React, { useEffect } from 'react'
import { withRouter } from 'react-router-dom'

// React icons
import { TiZoom } from "react-icons/ti";

const NotFound404 = ({history}) => {
    useEffect(() => {
        setTimeout(() => {
            history.push('/')
        }, [4000])
    }, [history])

    return (
        <div className="NotFound404__container">
            <h1>Pagina no encontrada</h1>
            <TiZoom/>
        </div>
    )
}

export default withRouter(NotFound404)
