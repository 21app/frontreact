// Estilos
import './NoAccess.scss'

// React
import React from 'react'

const NoAccess = () => {
    return (
        <div className="NoAccess__container">
            <h2>Para acceder a esta página por favor ingresa o registra una cuenta!</h2>
        </div>
    )
}

export default NoAccess
