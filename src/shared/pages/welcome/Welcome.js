// Componentes
import WelcomeCard from "../../../shared/components/ui/welcomeCard/WelcomeCard";

// Enum
import { ToastTypeEnum } from "../../../shared/enums/ToastTypeEnum";

// Estilos
import "./Welcome.scss";

// React
import React, { useEffect } from "react";

// React Icons
import { MdAccessibility, MdPeople, MdUpdate } from "react-icons/md";
import { TiLocationOutline, TiCalendar } from "react-icons/ti";

// Redux
import { connect } from "react-redux";
import {
  setToast,
  disactivateWelcomeMessage
} from "../../../store/actions/UIActions";
import CardLayout from "../../../shared/components/cardLayout/CardLayout";

// const logoImage = "./assets/images/corporateLogoBig.png";

const cards = [
  {
    name: "Partidos",
    path: "/matches",
    icon: <TiCalendar />,
    description: "Buscar, crear e inscibirte en partidos."
  },
  {
    name: "Desafiós",
    path: "/challenges",
    icon: <MdUpdate />,
    description: "Desafiós creados por nosotros y nuestros usuarios."
  },
  {
    name: "Canchas",
    path: "/fields",
    icon: <TiLocationOutline />,
    description: "Busca canchas por relevancia, estado, cercanía y reputación."
  },
  {
    name: "Equipos",
    path: "/teams",
    icon: <MdPeople />,
    description: "Arma tu equipo y busca rivales."
  },
  {
    name: "Jugadores",
    path: "/players",
    icon: <MdAccessibility />,
    description: "Busca los jugadores que necesitas para tu partido."
  }
];

const Welcome = ({
  setToast,
  disactivateWelcomeMessage,
  showWelcomeMessage
}) => {
  useEffect(() => {
    if (!showWelcomeMessage) return;
    setToast(ToastTypeEnum.Success, "Bienvenido!");
    disactivateWelcomeMessage();
  }, [setToast, disactivateWelcomeMessage, showWelcomeMessage]);

  return (
    <div className="Welcome__container">
      {/* Introducción */}
      <section className="Welcome__introduction">
        <div className="Welcome__logo">
          {/* <img src={logoImage} alt="Welcome-logo" /> */}
          <h1>Bienvenido a 21App</h1>
          <p>
            Crear partidos y desafiarte ahora es más facil que nunca. En nuestra
            plataforma tienes las herramientas necesarias para
            aprovechar tu tiempo y practicar el deporte rey.
          </p>
        </div>
      </section>
      {/* Tarjetas */}
      <section className="Welcome__cards">
        <CardLayout autoFillCards={false}>
          {cards.map((card, i) => (
              <WelcomeCard key={i} cardObject={card} />
          ))}
        </CardLayout>
      </section>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    showWelcomeMessage: state.uiReducer.showWelcomeMessage
  };
};

export default connect(
  mapStateToProps,
  { setToast, disactivateWelcomeMessage }
)(Welcome);
