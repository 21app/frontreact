import React, { useEffect } from "react";
import { withRouter } from "react-router-dom";

const NotFound404Handler = ({ history }) => {
  useEffect(() => {
    history.push("/404");
  }, [history]);
  return <React.Fragment />;
};

export default withRouter(NotFound404Handler);
