export const TeamsMockup = [
    {
      id: 0,
      name: "Panaderos",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Panaderos"
    },
    {
      id: 2,
      name: "Benjamín Herrera",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Benjamín Herrera"
    },
    {
      id: 3,
      name: "Carnicería la 31",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Carnicería la 31"
    },
    {
      id: 4,
      name: "Pizzeros de la esquina",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Pizzeros de la esquina"
    },
    {
      id: 5,
      name: "Los del mediodía",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Los del mediodía"
    },
    {
      id: 6,
      name: "Micro Experts",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Micro Experts"
    },
    {
      id: 7,
      name: "Villa Futbol 11",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Villa Futbol 11"
    },
    {
      id: 8,
      name: "The Definitive",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "The Definitive"
    },
    {
      id: 9,
      name: "Unipanamericana Fut5",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Unipanamericana Fut5"
    },
    {
      id: 10,
      name: "Innombrables",
      image: "https://img.fifa.com/image/upload/t_l4/u1pce41rataeqg29pxxs.jpg",
      description: "Innombrables"
    }
  ];
  