export const ChallengesMockup = [
  {
    id: 0,
    name: "21",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Patear el balón 21 veces sin dejar caer"
  },
  {
    id: 2,
    name: "Pegar al palo easy",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Pegarle al palo 3 veces seguidas desde 10m"
  },
  {
    id: 3,
    name: "Pegar al palo medium",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Pegarle al palo 3 veces seguidas desde 25m"
  },
  {
    id: 4,
    name: "Pegar al palo difficult",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Pegarle al palo 3 veces seguidas desde 40m"
  },
  {
    id: 5,
    name: "Cabecitas individuales",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Cabecear el balón sin dejarlo caer"
  },
  {
    id: 6,
    name: "Cabecitas grupales",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Cabecear el balón sin dejarlo caer pasandoló a compañeros"
  },
  {
    id: 7,
    name: "Gol desde esquina",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Hacer gol desde la esquina de la cancha"
  },
  {
    id: 8,
    name: "Sprint 40m",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Correr lo más rapido una distancia de 40m con el balón"
  },
  {
    id: 9,
    name: "Curva easy",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Curvear el balón y meter gol desde 10 metros"
  },
  {
    id: 10,
    name: "Curva difficult",
    image:
      "https://www.mundodeportivo.com/r/GODO/MD/p5/Barca/Imagenes/2018/07/19/Recortada/MUNDODEPORTIVO_G_3801644-kLR-U45964107135MLF-980x554@MundoDeportivo-Web.jpg",
    description: "Curvear el balón y meter gol desde 40 metros"
  }
];
