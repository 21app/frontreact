export const FieldsMockup = [
    {
      id: 0,
      name: "Villamayor",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 2,
      name: "Venecía",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 3,
      name: "Engativa",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 4,
      name: "Simón Bolivar",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 5,
      name: "Salitre",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 6,
      name: "Candelaría",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 7,
      name: "Usme",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 8,
      name: "Santa Fé",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 9,
      name: "Fontibón",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    },
    {
      id: 10,
      name: "Claret",
      image: "https://cdn.getyourguide.com/img/tour_img-2191252-146.jpg",
      description: "Cancha de cemento"
    }
  ];